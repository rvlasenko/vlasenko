<?php

use Tygh\Registry;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    
	if ($mode == 'email') {
        
		if (defined('AJAX_REQUEST')) {
			Tygh::$app['view']->assign('staff_id', $_REQUEST['staff_id']);
            Tygh::$app['view']->assign('email', fn_staff_get_email($_REQUEST['staff_id']));
            
			return array(CONTROLLER_STATUS_OK);
        }

    }

}
