<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

fn_define('KEEP_UPLOADED_FILES', true);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    //
    // Create/update employee
    //
    if ($mode == 'update') {

		if (!empty($_REQUEST['staff_data'])) {
		    $staff_id = fn_staff_update_employee($_REQUEST['staff_data'], $_REQUEST['staff_id'], DESCR_SL);

			if ($staff_id === false) {
                // Some error occured
                fn_save_post_data('staff_data');
		        fn_set_notification('E', __('error'), __('text_error_adding_request'));

		        return array(CONTROLLER_STATUS_REDIRECT, 'employee.manage');
            }
		}
    
	}

    //
    // Delete employee
    //
    if ($mode == 'delete') {

        if (!empty($_REQUEST['staff_id'])) {
            $result = fn_staff_remove_employee($_REQUEST['staff_id']);
			
			if ($result > 0) {
		        fn_set_notification('N', __('notice'), __('employee_has_been_deleted'));
		    } else {
		        return array(CONTROLLER_STATUS_REDIRECT, 'employee.manage');
		    }
		}

    }

    return array(CONTROLLER_STATUS_OK, 'employee.manage');

} 

//
// 'Staff management' page
//
if ($mode == 'manage') {

    $params = $_REQUEST;

    list($staff, $search) = fn_staff_load_employees_data($params, Registry::get('settings.Appearance.admin_elements_per_page'), DESCR_SL);

    Tygh::$app['view']->assign('staff', $staff);
    Tygh::$app['view']->assign('search', $search);
    Tygh::$app['view']->assign('object', 'global');

//
// 'Staff update' page
//
} elseif ($mode == 'update') {

    $staff_data = fn_staff_get_data($_REQUEST['staff_id']);

    if (!empty($_REQUEST['object'])) {
        Tygh::$app['view']->assign('object', $_REQUEST['object']);
    }

    Tygh::$app['view']->assign('staff_data', $staff_data);
    Tygh::$app['view']->assign('staff_id', $_REQUEST['staff_id']);

}
