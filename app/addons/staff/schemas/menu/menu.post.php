<?php

$schema['central']['website']['items']['staff'] = array(
    'attrs' => array(
        'class' => 'is-addon',
    ),
    'href' => 'employee.manage',
    'position' => 30,
);

return $schema;
