<?php

$schema['staff'] = array(
    'templates' => array(
        'addons/staff/blocks/staff.tpl' => array(
            'settings' => array(
                'not_scroll_automatically' => array(
                    'type' => 'checkbox',
                    'default_value' => 'Y'
                ),
                'scroll_per_page' => array(
                    'type' => 'checkbox',
                    'default_value' => 'N'
                ),
                'speed' => array(
                    'type' => 'input',
                    'default_value' => 500
                ),
                'outside_navigation' => array(
                    'type' => 'checkbox',
                    'default_value' => 'Y'
                ),
            ),
        ),
    ),
    'wrappers' => 'blocks/wrappers',
    'content' => array(
        'staff' => array(
            'type' => 'function',
            'function' => array('fn_staff_get_all_employees'),
        )
    ),
    'cache' => array(
        'update_handlers' => array(
            'staff', 
            'images_links',
        )
    ),
);

return $schema;
