<?php

use Tygh\Navigation\LastView;
use Tygh\Tools\SecurityHelper;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

/**
 * Gets all employees for block
 *
 * @return array
 */
function fn_staff_get_all_employees()
{
    $join = db_quote(' LEFT JOIN ?:users ON ?:users.user_id = ?:staff.user_id');
    
    $fields = array(
        '?:staff.staff_id',
        '?:staff.function',
        "IF (?:staff.firstname!='', ?:staff.firstname, ?:users.firstname) AS firstname",
        "IF (?:staff.lastname!='', ?:staff.lastname, ?:users.lastname) AS lastname",
        "IF (?:staff.email!='', ?:staff.email, ?:users.email) AS email",
    );

    $fields = implode(',', $fields);

    $staff = db_get_array("SELECT ?p FROM ?:staff ?p", $fields, $join);

    foreach ($staff as &$_staff) {
        $_staff['image_pair'] = fn_get_image_pairs($_staff['staff_id'], 'staff', 'M', true, true);
    }

    return $staff;
}

/**
 * Gets employee email by ID
 *
 * @param int $staff_id
 * @return sting
 */
function fn_staff_get_email($staff_id)
{
    $condition = db_quote('?:staff.staff_id = ?i', $staff_id);
    $join = db_quote(' LEFT JOIN ?:users ON ?:staff.user_id = ?:users.user_id');
    $fields = db_quote(" IF(?:staff.email!='', ?:staff.email, ?:users.email)");
    
    $staff_email = db_get_field("SELECT ?p FROM ?:staff ?p WHERE ?p ", $fields, $join, $condition);

    return $staff_email;
}

/**
 * Gets array of employees
 *
 * @param array $params
 * @param int $items_per_page
 * @param $lang_code
 * @return array
 */
function fn_staff_load_employees_data($params = array(), $items_per_page = 0, $lang_code = CART_LANGUAGE)
{
    $params = LastView::instance()->update('staff', $params);

    $default_params = array(
        'page' => 1,
        'items_per_page' => $items_per_page
    );

    $params = array_merge($default_params, $params);
    $join = db_quote(' LEFT JOIN ?:users ON ?:users.user_id = ?:staff.user_id');
    $fields = array(
        '?:staff.staff_id',
        '?:staff.function',
        '?:staff.user_id',
        "IF(?:staff.firstname!='', ?:staff.firstname, ?:users.firstname) AS firstname",
        "IF(?:staff.lastname!='', ?:staff.lastname, ?:users.lastname) AS lastname",
        "IF(?:staff.email!='', ?:staff.email, ?:users.email) AS email",
    );
    $fields = implode(',', $fields);
    
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(*) FROM ?:staff ?p", $join);
        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
    }

    $data = db_get_array("SELECT ?p FROM ?:staff ?p ?p", $fields, $join, $limit);

    return array($data, $params);
}

/**
 * Updates employee
 *
 * @param array $staff_data 
 * @param int $staff_id
 * @param string $lang_code
 * @return int
 */
function fn_staff_update_employee($staff_data, $staff_id = 0, $lang_code = DESCR_SL)
{
    SecurityHelper::sanitizeObjectData('staff', $staff_data);

    if (!empty($staff_data['user_id'])) {
        if (!db_get_field("SELECT COUNT(*) FROM ?:users LEFT JOIN ?:staff ON ?:staff.user_id = ?:users.user_id "
                        . "WHERE ?:users.user_id = ?i AND (?:staff.staff_id IS NULL OR ?:staff.staff_id = ?i)", $staff_data['user_id'], $staff_id)) {
            $staff_data['user_id'] = 0;
        }
    }

    if (empty($staff_id)) {
        $staff_data['staff_id'] = $staff_id = db_query('INSERT INTO ?:staff ?e', $staff_data);
        fn_attach_image_pairs('staff_image', 'staff', $staff_id, $lang_code);
    } else {
        db_query("UPDATE ?:staff SET ?u WHERE staff_id = ?i", $staff_data, $staff_id);
        fn_attach_image_pairs('staff_image', 'staff', $staff_id, $lang_code);
    }

    return $staff_id;
}

/**
 * Removes employee by ID
 *
 * @param int $staff_id
 * @return bool
 */
function fn_staff_remove_employee($staff_id)
{
    if (!empty($staff_id)) {
        $result = db_query("DELETE FROM ?:staff WHERE staff_id = ?i", $staff_id);
        fn_delete_image_pairs($staff_id, 'staff');
    }

    return $result;
}

/**
 * Gets staff data by ID
 *
 * @param int $staff_id
 * @param $lang_code
 * @return array
 */
function fn_staff_get_data($staff_id, $lang_code = DESCR_SL)
{
    if (!empty($staff_id)) {
        $staff = db_get_row("SELECT * FROM ?:staff WHERE staff_id = ?i", $staff_id);

        if (!empty($staff)) {
            $staff['image_pair'] = fn_get_image_pairs($staff_id, 'staff', 'M', true, true, $lang_code);
        }
    }

    return $staff;
}
