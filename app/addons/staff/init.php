<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

fn_register_hooks(
    'get_all_employees',
    'get_email',
	'update_employee',
	'load_employees_data',
	'remove_employee',
	'get_data'
);