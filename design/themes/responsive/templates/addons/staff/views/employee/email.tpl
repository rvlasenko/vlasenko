<div id="user_email_{$staff_id}">
	{if empty($email)}
		{__("staff_email_not_found")}
	{else}
		<a href="mailto:{$email}">{$email}</a>
	{/if}<!-- here -->
</div>
