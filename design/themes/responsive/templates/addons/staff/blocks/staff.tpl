{$obj_prefix = "`$block.block_id`000"}
{** block-description:staff_block **}

{if $block.properties.outside_navigation == "Y"}
    <div class="owl-theme ty-owl-controls">
        <div class="owl-controls clickable owl-controls-outside" id="owl_outside_nav_{$block.block_id}">
            <div class="owl-buttons">
                <div id="owl_prev_{$obj_prefix}" class="owl-prev"><i class="ty-icon-left-open-thin"></i></div>
                <div id="owl_next_{$obj_prefix}" class="owl-next"><i class="ty-icon-right-open-thin"></i></div>
            </div>
        </div>
    </div>
{/if}

<div id="scroll_list_{$block.block_id}" class="owl-carousel" style="margin: 20px 0 20px;">
    {foreach from=$staff item="man" name="for_staff"}
        <div class="ty-scroller-list__item">
            {include file="common/image.tpl" 
                assign="object_img" 
                image_width=$block.properties.thumbnail_width 
                image_height=$block.properties.thumbnail_width 
                images=$man.image_pair 
                no_ids=true 
                lazy_load=true 
                obj_id="scr_`$block.block_id`000`$man.staff_id`"}
            {$object_img nofilter}

            <h4 style="margin: 10px 0 0 0;">{$man.firstname} {$man.lastname}</h4>
            <p>{$man.function}</p>
            <div id="user_email_{$man.staff_id}">
                <a href="{"employee.email?staff_id=`$man.staff_id`"|fn_url}" class="cm-ajax cm-post" data-ca-target-id="user_email_{$man.staff_id}">{__("staff_show_email")}</a>
            </div>
         </div>
   {/foreach}
</div>

{include file="common/scroller_init.tpl" items=$staff prev_selector="#owl_prev_`$obj_prefix`" next_selector="#owl_next_`$obj_prefix`"}