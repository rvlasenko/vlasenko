{capture name="mainbox"}

    {include file="common/pagination.tpl"}

    <div class="items-container id="manage_staff_list">
        {if $staff}
        <table width="100%" class="table table-middle table-striped">
            <thead>
                <tr>
                    <th class="left" width="1%">{include file="common/check_items.tpl"}</th>
                    <th width="20%">{__("first_name")} {__("and")} {__("last_name")|lower}</th>
                    <th>&nbsp;</th>
                    <th width="25%">{__("staff_function")}</th>
                    <th>{__("email")}</th>
                    <th>{__("user")}</th>
                </tr>
            </thead>
            
            <tbody>
                {foreach from=$staff item=man}
                <tr data-ct-staff="{$man.staff_id}">
                    <td class="left">
                        <input type="checkbox" class="cm-item" value="{$tag.tag_id}" name="tag_ids[]"/>
                    </td>
                    <td>
                        {$man.firstname|default:" - "} {$man.lastname|default:" - "}
                    </td>
                    <td>
                        {capture name="tools_list"}
                            <li>{include file="common/popupbox.tpl" id="group_staff_`$man.staff_id`" text="{__("staff_editing")}: `$man.firstname` `$man.lastname`" act="edit" link_text=$link_text href="employee.update?staff_id=`$man.staff_id`"}</li>
                            <li>{btn type="list" class="cm-confirm cm-post" text=__("delete") href="employee.delete?staff_id=`$man.staff_id`"}</li>
                        {/capture}
                        <div class="hidden-tools">
                            {dropdown content=$smarty.capture.tools_list}
                        </div>
                    </td>
                    <td>
                        {$man.function}
                    </td>
                    <td>
                        <a href="mailto:{$man.email}" title="Откроется почтовый клиент">{$man.email}</a>
                    </td>
                    <td>
                        {if !empty($man.user_id)}<a href="{"profiles.update&user_id=`$man.user_id`"|fn_url}" title="Редактирование данных">ID привязан (#{$man.user_id})</a>
                        {else} ID не привязан {/if}
                    </td>
                </tr>
                {/foreach}
            </tbody>
        </table>
        {else}
            <p class="no-items">{__("no_data")}</p>
        {/if}
    </div>

    {include file="common/pagination.tpl"}

{/capture}

{capture name="adv_buttons"}
    {capture name="add_new_picker"}
        {include file="addons/staff/views/employee/update.tpl" option_id="0"}
    {/capture}
    {include file="common/popupbox.tpl" id="add_new_employee" text=__("staff_new") title=__("staff_add") act="general" content=$smarty.capture.add_new_picker icon="icon-plus"}
{/capture}

{include file="common/mainbox.tpl" title=__("staff") content=$smarty.capture.mainbox adv_buttons=$smarty.capture.adv_buttons select_language=true}
